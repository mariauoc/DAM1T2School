<!DOCTYPE html>
<!--
Página que permite registrar un proyecto
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Proyecto</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="POST">
            <p>Nombre del proyecto: <input type="text" name="nombre"></p>
            <p>Fecha: <input type="date" name="fecha"></p>
            <p>Nota: <input type="number" name="nota" min="0" max="10"></p>
            <p>Código del alumno: 
                <select name="alumno">
                    <?php
                    $codigos = selectCodeAlumnos();
                    while ($fila = mysqli_fetch_assoc($codigos)) {
                        echo "<option>";
                        echo $fila["code"];
                        echo "</option>";
                    }
                    ?>
                </select>
            </p>
            <input type="submit" name="boton" value="Alta">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $nombre = $_POST["nombre"];
            $fecha = $_POST["fecha"];
            $nota = $_POST["nota"];
            $alumno = $_POST["alumno"];
            $resultado = insertar_proyecto($nombre, $fecha, $nota, $alumno);
            if ($resultado == "ok") {
                echo "Proyecto dado de alta" ;
            } else {
                echo "ERROR: $resultado";
            }
        }
        ?>
    </body>
</html>
