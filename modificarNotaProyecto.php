<!DOCTYPE html>
<!--
Modificar la nota de un proyecto
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="POST">
            Selecciona un proyecto: <select name="proyecto">
                <?php
                $codigos = selectIdProjects();
                while ($fila = mysqli_fetch_assoc($codigos)) {
                    echo "<option>";
                    echo $fila["idproject"];
                    echo "</option>";
                }
                ?>
            </select>
            <br>
            Introduce la nueva nota: 
            <input type="number" name="nota" min="0" max="10" required>
            <br>
            <input type="submit" value="Modificar" name="boton">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $id= $_POST["proyecto"];
            $nota = $_POST["nota"];
            $resultado = updateMarkProject($id, $nota);
            if ($resultado = "ok") {
                echo "Nota actualizada";
            } else {
                echo "ERROR: $resultado";
            }
        }
        ?>
    </body>
</html>
