<!DOCTYPE html>
<!--
Página que permite seleccionar el proyecto a modificar (Primer parte)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
         <form method="POST" action="modificar2.php">
            Selecciona un proyecto para modificar la nota: <select name="proyecto">
                <?php
//                $codigos = selectIdProjects();
                $datos = selectIdMarkProjects();
                while ($fila = mysqli_fetch_assoc($datos)) {
                    echo "<option value='".$fila["idproject"]."'>";
                    echo "Proyecto: ". $fila["idproject"]." Nota: ".$fila["mark"];
                    echo "</option>";
                }
                ?>
            </select>
            <input type="submit" value="Seleccionar" name="boton">
        </form>
    </body>
</html>
