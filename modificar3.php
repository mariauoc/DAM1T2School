<!DOCTYPE html>
<!--
Página que debe recibir el proyecto y la nota y modificarla
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        $proyecto = $_POST["proyecto"];
        $nota = $_POST["nota"];
        $resultado = updateMarkProject($proyecto, $nota);
        if ($resultado == "ok") {
            echo "Nota modificada";
        } else {
            echo "ERROR: $resultado";
        }
        ?>
    </body>
</html>
