<?php

/*
 * Fichero donde tendremos las funciones que tengan que ver con la bbdd
 */

// Función que devuelve la nota de un proyecto
function selectMarkByIdProyecto($proyecto) {
    $c = conectar();
    $select = "select mark from project where idproject = $proyecto";
    $resultado = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($resultado);
    desconectar($c);
    return $fila["mark"];
}

// Función que devuelve id y nota de todos los proyectos
function selectIdMarkProjects() {
    $c = conectar();
    $select = "select idproject, mark from project order by idproject";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve todos los id de los proyectos
function selectIdProjects() {
    $c = conectar();
    $select = "select idproject from project order by idproject";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve los códigos de los alumnos
function selectCodeAlumnos() {
    $c = conectar();
    $select = "select code from student";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve todos los datos de todos los
// alumnos que están en la base de datos
function selectAllAlumnos() {
    // conectamos con la bbdd
    $c = conectar();
    // Preparamos la consulta
    $select = "select * from student";
    // Ejecutamos la consulta y recogemos el resultado
    $resultado = mysqli_query($c, $select);
    // desconectamos de la bbdd
    desconectar($c);
    // Devolvemos el resultado
    return $resultado;
}

// Función que modifica la nota de un proyecto determinado
function updateMarkProject($idproyecto, $nuevanota) {
    $c = conectar();
    $update = "update project set mark=$nuevanota 
            where idproject=$idproyecto";
    if (mysqli_query($c, $update)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que inserta un proyecto de un alumno en la bbdd
function insertar_proyecto($name, $fecha, $nota, $alumno) {
    $c = conectar();
    $insert = "insert into project values (null, '$name', 
                '$fecha', $nota, $alumno)";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que inserta los datos de un alumno en la bbdd
function insertar_alumno($codigo, $nombre, $apellidos, $edad, $genero) {
    // conectamos con la bbdd
    $c = conectar();
    // Preparamos la consulta
    $insert = "insert into student values ($codigo, '$nombre', 
            '$apellidos', $edad, '$genero')";
    // Ejecutamos la consulta
    if (mysqli_query($c, $insert)) {
        // Si ha ido bien devolveremos ok
        $resultado = "ok";
    } else {
        // Sino devolvemos el mensaje de error
        $resultado = mysqli_error($c);
    }
    // Cerramos la conexión
    desconectar($c);
    // Devolvemos el msg (ok o error)
    return $resultado;
}

// Función que conecta a la base de datos 
function conectar() {
    $conexion = mysqli_connect("localhost", "root", "root", "school");
    // Si no ha ido bien la conexión
    if (!$conexion) {
        die("No se ha podido establecer la conexión");
    }
    return $conexion;
}

// Función que cierra una conexión con la base de datos
function desconectar($conexion) {
    mysqli_close($conexion);
}
