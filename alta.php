<!DOCTYPE html>
<!--
Registrar alumno
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nuevo Alumno</title>
    </head>
    <body>
        <h2>Datos del alumno</h2>
        <form action="" method="POST">
            Código: <input type="number" name="codigo" required><br>
            Nombre: <input type="text" name="nombre" required ><br>
            Apellidos: <input type="text" name="apellidos" required><br>
            Edad: <input type="number" name="edad" required=""><br>
            Género: <input type="radio" name="genero" value="Hombre" required> Hombre
            <input type="radio" name="genero" value="Mujer" required> Mujer
            <br>
            <input type="submit" value="Registrar" name="boton" >
        </form>
        
        <?php
        require_once 'bbdd.php';
        if (isset($_POST["boton"])) {
            $codigo = $_POST["codigo"];
            $nombre = $_POST["nombre"];
            $apellidos = $_POST["apellidos"];
            $edad = $_POST["edad"];
            $genero = $_POST["genero"];
            $registro = insertar_alumno($codigo, $nombre, $apellidos, $edad, $genero);
            if ($registro == "ok") {
                echo "<p>Alumno registrado en la base de datos</p>";
            } else {
                echo "Error al registrar el alumno $registro<br>";
            }
        }
        ?>
        <p><a href="index.php">Volver al menú principal</a></p>
    </body>
</html>
