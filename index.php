<!DOCTYPE html>
<!-- Primer ejemplo de CRUD con BBDD desde PHP
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>School</title>
    </head>
    <body>
       <h1>Stucom, la aplicación para registrar alumnos</h1>
       <p></p>
       <p><a href="alta.php">Registrar un alumno</a></p>
       <p><a href="altaProyecto.php">Registrar un proyecto</a></p>
       <p><a href="modificarNotaProyecto.php">Modificar la nota de un proyecto</a></p>
       <p><a href="modificarProyecto.php">Modificar la nota de un proyecto versión 2.0</a></p>
       <p><a href="listado.php">Ver todos los datos de los alumnos</a></p>
    </body>
</html>
