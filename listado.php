<!DOCTYPE html>
<!--
 Página que muestra todos los datos de los alumnos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Alumnos</title>
    </head>
    <body>
        <h2>Datos de los alumnos</h2>
        <?php
        // Incluimos el fichero
        require_once 'bbdd.php';
        // Ejecutamos la consulta y recogemos el resultado en $alumnos
        $alumnos = selectAllAlumnos();
        // Abrimos la tabla en html
        echo "<table>";
        // Mostramos los títulos de la cabecera
        echo "<tr>";
        echo "<th>Código</th><th>Nombre</th><th>Apellidos</th><th>Edad</th><th>Género</th>";
        echo "</tr>";
        // Mostramos los datos fila a fila
        while ($fila = mysqli_fetch_assoc($alumnos)) {
            echo "<tr>";
            foreach ($fila as $valor) {
                echo "<td>$valor</td>";
            }
            echo "</tr>";
            
//            // Extraemos los datos de la fila actual
//            extract($fila);
//            echo "<tr>";
//            // Extract nos crea las variables $code,$name, $surname, etc...
//            echo "<td>$code</td> <td>$name</td> <td>$surname</td> <td>$age</td>
//                     <td>$gender</td>";
//            echo "</tr>";
        }
        echo "</table>";
        ?>
    </body>
</html>
