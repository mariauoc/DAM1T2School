<!DOCTYPE html>
<!--
Recibe el proyecto modificado, consulta la nota y la muestra para 
que pueda ser modificada
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        $proyecto = $_POST["proyecto"];
        $nota = selectMarkByIdProyecto($proyecto);
        ?>
        <form action="modificar3.php" method="POST">
            Nueva nota: 
            <input type="number" name="nota" value="<?php echo $nota?>">
            <input type="hidden" name="proyecto" value="<?php echo $proyecto?>">
            <input type="submit" value="Modificar nota">
        </form>
    </body>
</html>
